// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "SpaceshipBase.generated.h"

/*
*	Acts as a base for the game. The base will have Health and 
*	take damage from enemies.
*	If the base runs out of health, it's game over.
*	If the game timer runs out before base has no health,
*	player wins.
*/
UCLASS()
class A3PROJ_API ASpaceshipBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpaceshipBase();
	UPROPERTY(BlueprintReadOnly)
		int32 Health;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MaxHealth;

	UPROPERTY(EditAnywhere)
		UParticleSystem* Explosion;

	UPROPERTY(EditAnywhere)
		USoundCue* BreakSound;

	UPROPERTY(EditAnywhere)
		UBoxComponent* ShipCollision;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* ShipMesh;

	FTimerHandle BeforeGameOver; // puase before game over called
	bool Exploded;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void TakeHit(int32 Damage);
	bool Explode();
	void GameOver();
};
