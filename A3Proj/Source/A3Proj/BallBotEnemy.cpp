// Fill out your copyright notice in the Description page of Project Settings.


#include "BallBotEnemy.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"

ABallBotEnemy::ABallBotEnemy()
{
	PrimaryActorTick.bCanEverTick = true;
	ConstructorHelpers::FObjectFinder<UStaticMesh> SEnemyMesh(TEXT("/Game/Geometry/Meshes/EnemyBallMesh.EnemyBallMesh"));

	if (SEnemyMesh.Succeeded())
		EnemyMesh->SetStaticMesh(SEnemyMesh.Object);

	// Defaults
	Size = 1;
	MaxHealth = 10;
}

void ABallBotEnemy::BeginPlay()
{
	Super::BeginPlay();
}

void ABallBotEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
