// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "A3ProjGameMode.h"
#include "A3ProjCharacter.h"
#include "UObject/ConstructorHelpers.h"

AA3ProjGameMode::AA3ProjGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
