// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseEnemy.h"
#include "ConeRobotEnemy.generated.h"

/**
 *	Larger than average enemy with slightly slower speed
 *	a little more health and damage.
 */
UCLASS()
class A3PROJ_API AConeRobotEnemy : public ABaseEnemy
{
	GENERATED_BODY()

	protected:
		virtual void BeginPlay() override;

	public:
		AConeRobotEnemy();
		virtual void Tick(float DeltaTime) override;
};
