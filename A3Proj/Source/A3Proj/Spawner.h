// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseEnemy.h"
#include "ShurikenEnemy.h"
#include "ConeRobotEnemy.h"
#include "Spawner.generated.h"


/*
*	Spawner instantiates enemies of class BaseEnemy and passes
*	them a range of paths within the world to follow.
*	Enemy creation is dependant on a timer amoung other conditions.
*	Once the spawner has spawned the requested quanity of enemies,
*	the spawner will deactive.
*/
UCLASS()
class A3PROJ_API ASpawner : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		float SpawnInterval;	// seconds until next spawn

	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<ABaseEnemy>> EnemyTypes; // possible enemies that can be spawned

	UPROPERTY(EditAnywhere)
		int32 MaxSpawnCount;

	TArray<FPath> EnemyPaths;
	float SpawnerCountDown;
	int32 NumberEnemiesSpawned; // count to end of round
	int32 MaxNumberEnemiesOnScreen;

	// For waypoint generation
	FVector StartWaypoint = FVector(-2217, 1461, 187);
	float IncrementX = 720;
	float DecrementY = 855;
	int32 PathNumber = 4;
	int32 WaypointNumber = 6;
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

	UPROPERTY(BlueprintReadWrite)
		bool StartSpawning; // can be used to pause between waves

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void SpawnEnemy();
	void GeneratePaths();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
