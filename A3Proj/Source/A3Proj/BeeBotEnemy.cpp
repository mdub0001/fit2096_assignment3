// Fill out your copyright notice in the Description page of Project Settings.


#include "BeeBotEnemy.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"

ABeeBotEnemy::ABeeBotEnemy()
{
	PrimaryActorTick.bCanEverTick = true;
	ConstructorHelpers::FObjectFinder<UStaticMesh> SEnemyMesh(TEXT("/Game/Geometry/Meshes/EnemyBeeMesh.EnemyBeeMesh"));

	if (SEnemyMesh.Succeeded())
		EnemyMesh->SetStaticMesh(SEnemyMesh.Object);

	// Defaults
	Size = 1;
	MaxHealth = 10;
}

void ABeeBotEnemy::BeginPlay()
{
	Super::BeginPlay();
}

void ABeeBotEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
