// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseTower.h"
#include "BaseEnemy.h"
#include "GameFramework/PlayerController.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Materials/MaterialInstanceDynamic.h"

ABaseTower::ABaseTower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	Scene->SetupAttachment(RootComponent);
	TowerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	TowerMesh->SetupAttachment(Scene);
	CollisionVolume = CreateDefaultSubobject<USphereComponent>(TEXT("Tower Collision"));
	CollisionVolume->SetupAttachment(Scene);

	// Set up particle explosion
	ConstructorHelpers::FObjectFinder<UParticleSystem> ExplosionPath = TEXT("/Game/StarterContent/Particles/P_Explosion.P_Explosion");
	if (ExplosionPath.Succeeded())
		TowerExplosion = ExplosionPath.Object;

	// Defaults
	DetectionRadius = 30;
	MinAttackDamage = 5;
	MaxAttackDamage = 10;
	MaxHealth = 20;
	DamageScore = 2;
}

// Called when the game starts or when spawned
void ABaseTower::BeginPlay()
{
	Super::BeginPlay();
	Health = MaxHealth;
	// create material instance for colour change
	TowerMaterial = UMaterialInstanceDynamic::Create(TowerMesh->GetMaterial(0), NULL);
	TowerMesh->SetMaterial(0, TowerMaterial);
}

// Called every frame
void ABaseTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

/*
*	Take damage from enemies that collide and explode
*	next to the tower.
*	Slowly turn red as Health drops.
*	If health goes below 0, tower is destroyed
*/
void ABaseTower::TakeHit(int32 Damage)
{
	if (Damage >= Health)
	{
		Die();
		return;
	}

	Health -= Damage;
	float matval = (float(Health) / MaxHealth);
	TowerMaterial->SetScalarParameterValue(EmmisiveChangeAttribute, matval);
}

/*
*	Towers have differing shoot functionality.
*/
void ABaseTower::Shoot()
{

}

/*
*	Upon death, spawn an explosion then destroy self
*/
void ABaseTower::Die()
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TowerExplosion, GetActorTransform(), true, EPSCPoolMethod::AutoRelease, true);
	Destroy();
}
