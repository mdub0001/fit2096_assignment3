// Fill out your copyright notice in the Description page of Project Settings.


#include "ShurikenEnemy.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"

AShurikenEnemy::AShurikenEnemy()
{
	PrimaryActorTick.bCanEverTick = true;
	ConstructorHelpers::FObjectFinder<UStaticMesh> SEnemyMesh(TEXT("/Game/Geometry/Meshes/ShurikenMesh.ShurikenMesh"));

	if (SEnemyMesh.Succeeded())
		EnemyMesh->SetStaticMesh(SEnemyMesh.Object);

	// Defaults
	Size = 0.5;
	MaxHealth = 10;
}

void AShurikenEnemy::BeginPlay()
{
	Super::BeginPlay();
}

void AShurikenEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
