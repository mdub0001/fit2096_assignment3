// Fill out your copyright notice in the Description page of Project Settings.


#include "AOETower.h"
#include "BaseEnemy.h"
#include "TDGameMode.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"

AAOETower::AAOETower()
{
	PrimaryActorTick.bCanEverTick = true;
	ConstructorHelpers::FObjectFinder<UStaticMesh> CrystalMeshPath(TEXT("/Game/Geometry/Meshes/CrystalTower.CrystalTower"));
	ConstructorHelpers::FObjectFinder<UMaterialInterface> DecalMatSource(TEXT("/Game/Materials/M_AOEDecal.M_AOEDecal"));
	AOEEffectDecal = CreateDefaultSubobject<UDecalComponent>("Decal");

	if (CrystalMeshPath.Succeeded())
		TowerMesh->SetStaticMesh(CrystalMeshPath.Object);

	// prevent decal from projecting onto actor mesh
	TowerMesh->SetReceivesDecals(false);

	if (DecalMatSource.Succeeded())
		AOEEffectDecal->SetMaterial(0, DecalMatSource.Object);

	// Defaults
	RateOfFire = 3;
	CoolDownTimer = 2;
}

void AAOETower::BeginPlay()
{
	Super::BeginPlay();

	// set decal values
	AOEEffectDecal->SetWorldLocation(GetActorLocation(), false, nullptr, ETeleportType::None);
	AOEEffectDecal->SetWorldRotation(FRotator(90), false, nullptr, ETeleportType::None);
	AOEEffectDecal->SetWorldScale3D(FVector(DetectionRadius/100, DetectionRadius/100, DetectionRadius/100));
}

void AAOETower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CoolDownTimer -= DeltaTime;

	// controls rate of fire
	if (CoolDownTimer < 0)
	{
		Shoot();
		CoolDownTimer = RateOfFire;	
	}
}

/*
*	Generates a sphere, getting all potential targets within that sphere.
*	Sphere size is determined by set enemy range.
*	Does damage to those targets if they are enemies and updates the players
*	score.
*/
void AAOETower::Shoot()
{
	TArray<AActor*> OutActors;
	TArray<AActor*> Ignore;
	Ignore.Add(this);
	TArray<TEnumAsByte<EObjectTypeQuery>> Types;
	Types.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic)); // only check for dynamic collisions

	// Get all actors within the sphere.
	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), GetActorLocation(), DetectionRadius, Types, NULL, Ignore, OutActors);

	// if any actor is an enemy, damage and update player score.
	for (int i =  0; i < OutActors.Num(); i++)
	{
		if (!IsValid(OutActors[i]))
			return;

		ABaseEnemy* Enemy = Cast<ABaseEnemy>(OutActors[i]);
		if (Enemy)
		{
			Enemy->TakeHit(FMath::RandRange(MinAttackDamage, MaxAttackDamage));
			ATDGameMode* Gmode = Cast<ATDGameMode>(GetWorld()->GetAuthGameMode()); // 2 score points for damage
			Gmode->UpdateScore(DamageScore);
		}
	}
}
