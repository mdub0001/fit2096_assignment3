// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseTower.h"
#include "Engine/DecalActor.h"
#include "Components/DecalComponent.h"
#include "AOETower.generated.h"

class ABaseEnemy;

/**
 *	A tower that does AOE damage to all enemy actors within a given
 *	Radius.
 *	Radius is (roughly) depicted by the decal that spawns beneath the tower.
 */
UCLASS()
class A3PROJ_API AAOETower : public ABaseTower
{
	GENERATED_BODY()
	
	protected:
		virtual void BeginPlay() override;

	public:
		AAOETower();
		virtual void Tick(float DetlaTime) override;
		virtual void Shoot();

		// Decal spawns beneath the tower to display (approx) radius
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			UDecalComponent* AOEEffectDecal;
};
