// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpaceshipBase.h"
#include "Sound/SoundCue.h"
#include "Components/BoxComponent.h"
#include "BaseEnemy.generated.h"

class UMaterialInstanceDynamic;
class ABaseTower;

/*
*	Struct holds a single path. Path is an array of FVectors which
*	the enemies will navigate to one by one until final waypoint is reached.
*	Struct created as it isn't possible to hold a TArray within another TArray,
*	but it is possible to have a TArray holding stucts which holds a TArray.
*/
USTRUCT()
struct FPath
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere)
		TArray<FVector> Waypoints;
};

/*
*	Base functionality of enemies in game.
*	Enemies seek to find their foe and sacrifice themselves in
*	an attempt to prevent the player from winning.
*	Will only explode itself on Towers or the ShipBase.
*	Enemies have knowledge of array of waypoints that will be 
*	used to traverse the map.
*	Enemies can be killed by towers before killing themselves, 
*	meaning they do no harm to player friendly actors.
*	Enemies will move at a speed based on their size, 
*	larger enemies move slower.
*/
UCLASS(ABSTRACT)
class A3PROJ_API ABaseEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 ExplosionDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 DeathScorePoints; // point allocated to the player if killed

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(UIMin = '0', UIMax = '3')) // not 3 to prevent 0 speed
		float Size;	// of enemy mesh

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MaxHealth; // set by user

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* EnemyMesh;

	UPROPERTY(EditAnywhere)
		UBoxComponent* EnemyCollision;

	UPROPERTY(EditAnywhere)
		UParticleSystem* EnemyExplosion;

	UPROPERTY(EditAnywhere)
		USoundCue* ExplosionSound;

	// Allows for colour change
	UMaterialInstanceDynamic* EnemyMaterial;
	FName ColourChangeAttribute = TEXT("Blend");

	// Map of waypoints
	TArray<FPath> *Paths;
	int32 MaxWaypointNumber;
	int32 PathNumber;
	int32 WaypointNumber;

	// basic attributes
	int32 Health;
	float Speed;
	float MaxSpeed;
	float CurrentSpeed;
	float TurnSpeed;
	float Acceleration;
	float MaxSize;
	float MinimumDistance;	// from target waypoint
	bool IsTurning;
	float TurnThreshold;

	FTimerHandle DeathTimer;
	bool Dead;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void InitializePaths(TArray<FPath> *MapPaths, int32 nPathNumber, int32 nMaxWaypointNumber);
	void Die();
	void DecidePath();
	void TakeHit(int32 Damage);

	UFUNCTION()
		void OnBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
