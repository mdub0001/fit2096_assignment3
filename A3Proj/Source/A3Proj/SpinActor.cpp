// Fill out your copyright notice in the Description page of Project Settings.


#include "SpinActor.h"
#include "Components/StaticMeshComponent.h"

ASpinActor::ASpinActor()
{
	PrimaryActorTick.bCanEverTick = true;

	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);

	// Defaults
	RotationSpeed = 40.0f;
}

// Called when the game starts or when spawned
void ASpinActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASpinActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Rotate
	FRotator Rotator(0, RotationSpeed * DeltaTime, 0);
	FQuat RotationQuat(Rotator);
	AddActorLocalRotation(RotationQuat, false, nullptr, ETeleportType::None);
}

