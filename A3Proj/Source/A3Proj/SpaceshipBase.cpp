// Fill out your copyright notice in the Description page of Project Settings.


#include "SpaceshipBase.h"
#include "UObject/ConstructorHelpers.h"
#include "TDGameMode.h"
#include "Particles/ParticleSystemComponent.h"

ASpaceshipBase::ASpaceshipBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	Scene->SetupAttachment(RootComponent);
	ShipMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	ShipMesh->SetupAttachment(Scene);
	ShipCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Ship Collision"));
	ShipCollision->SetupAttachment(Scene);

	// Plays on ship destruction
	ConstructorHelpers::FObjectFinder<USoundCue> BreakSoundPath = TEXT("/Game/StarterContent/Audio/Collapse_Cue.Collapse_Cue");
	if (BreakSoundPath.Succeeded())
		 BreakSound = BreakSoundPath.Object;

	ConstructorHelpers::FObjectFinder<UParticleSystem> ExplosionP = TEXT("/Game/StarterContent/Particles/P_Explosion.P_Explosion");
	if (ExplosionP.Succeeded())
		Explosion = ExplosionP.Object;

	// Defaults
	MaxHealth = 40;
}

// Called when the game starts or when spawned
void ASpaceshipBase::BeginPlay()
{
	Super::BeginPlay();
	Health = MaxHealth;
}

// Called every frame
void ASpaceshipBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

/*
*	Called when an enemy deals damage to the Ship base.
*	If the health of the enemy goes below 0, ship explodes and
*	Game Over occours.
*/
void ASpaceshipBase::TakeHit(int32 Damage)
{
	if (Exploded)
		return;

	if (Damage >= Health)
		Exploded = Explode();

	Health -= Damage;
}

/*
*	Called when the ship runs out of health.
*	Creates explosion and triggers game over.
*	Returns true if exploded.
*/
bool ASpaceshipBase::Explode()
{
	// Set particle parameters
	FTransform splosion;
	splosion.SetLocation(GetActorLocation());
	splosion.SetRotation(FQuat(FRotator(0)));
	splosion.SetScale3D(FVector(6,6,6)); // Need a BIG explosion

	// Spawn Explosion Particle and sound
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Explosion, splosion, true, EPSCPoolMethod::AutoRelease, true);
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), BreakSound, GetActorLocation(), FRotator::ZeroRotator, 1, 1, 0, nullptr, nullptr, false);
	GetWorld()->GetTimerManager().SetTimer(BeforeGameOver, this, &ASpaceshipBase::GameOver, 2, false); // pause before game over
	return true;
}

/*
*	Calls the game mode to end game.
*	Destroys this ship.
*/
void ASpaceshipBase::GameOver()
{
	ATDGameMode* Gmode = Cast<ATDGameMode>(GetWorld()->GetAuthGameMode());
	Gmode->SetGameOver(true);
	Destroy();
}
