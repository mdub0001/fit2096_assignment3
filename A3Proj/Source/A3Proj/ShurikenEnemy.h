// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseEnemy.h"
#include "ShurikenEnemy.generated.h"

/*
*	Shuriken enemy. Fast but low HP and doesn't
*	deal much damage.
*/
UCLASS()
class A3PROJ_API AShurikenEnemy : public ABaseEnemy
{
	GENERATED_BODY()

	protected:
		virtual void BeginPlay() override;

	public:
		AShurikenEnemy();
		virtual void Tick(float DeltaTime) override;	
};
