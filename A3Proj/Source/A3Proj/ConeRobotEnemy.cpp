// Fill out your copyright notice in the Description page of Project Settings.


#include "ConeRobotEnemy.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"

AConeRobotEnemy::AConeRobotEnemy()
{
	PrimaryActorTick.bCanEverTick = true;
	ConstructorHelpers::FObjectFinder<UStaticMesh> LEnemyMesh(TEXT("/Game/Geometry/Meshes/RobotMesh.RobotMesh"));

	if (LEnemyMesh.Succeeded())
		EnemyMesh->SetStaticMesh(LEnemyMesh.Object);

	// Defaults
	Size = 2.3;
	MaxHealth = 10;
}

void AConeRobotEnemy::BeginPlay()
{
	Super::BeginPlay();
	Health = MaxHealth;
}

void AConeRobotEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
