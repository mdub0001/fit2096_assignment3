// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"
#include "TDGameMode.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"

ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	Scene->SetupAttachment(RootComponent);
	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	BulletMesh->SetupAttachment(Scene);
	BulletCollision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Bullet Collision"));
	BulletCollision->SetupAttachment(Scene);
	BulletSparks = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet Sparks"));
	BulletSparks->SetupAttachment(Scene);

	ConstructorHelpers::FObjectFinder<UStaticMesh> Capsule(TEXT("/Game/Geometry/Meshes/BulletMesh.BulletMesh"));
	if (Capsule.Succeeded())
		BulletMesh->SetStaticMesh(Capsule.Object);

	// Set up particle trail
	ConstructorHelpers::FObjectFinder<UParticleSystem> ShootTrailPath = TEXT("/Game/StarterContent/Particles/P_BulletTrail.P_BulletTrail");
	if (ShootTrailPath.Succeeded())
		 BulletSparks->SetTemplate(ShootTrailPath.Object);

	// set up pew pew sound
	ConstructorHelpers::FObjectFinder<USoundCue> ShootSoundPath = TEXT("/Game/StarterContent/Audio/Pew_Cue.Pew_Cue");
	if (ShootSoundPath.Succeeded())
		 ShootSound = ShootSoundPath.Object;

	// Defaults
	BulletSparks->bAutoDestroy = true;
	LifeRange = 30;
	Speed = 2000.0;
	Damage = 5;
	DamageScore = 0;
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();

	BulletCollision->OnComponentBeginOverlap.AddDynamic(this, &ABullet::OnCapsuleOverlapBegin);
	SpawnLocation = GetActorLocation();
	this->SetActorScale3D(FVector(10, 10, 10));

	// Play shooting sound at spawn position
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ShootSound, GetActorLocation(), FRotator::ZeroRotator, 1, 1, 0, nullptr, nullptr, false);

	// set bullet facing direction
	FVector Forward = FVector(1, 0, 0);
	float Dot = FVector::DotProduct(Forward, Direction);
	float Det = Forward.X * Direction.Y + Forward.Y * Direction.X;
	float RotationRad = FMath::Atan2(Det, Dot);
	float Degrees = FMath::RadiansToDegrees(RotationRad);
	SetActorRotation(FRotator(0, Degrees, 0));
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// if bullet travels beyond possible range, despawn
	FVector CurrentLoc = GetActorLocation();
	if (FVector::Dist(CurrentLoc, SpawnLocation) > LifeRange)
	{
		BulletSparks->DestroyComponent();
		Destroy();
	}

	// travel within the given direction
	FVector NLocation = CurrentLoc + (Direction * Speed) * DeltaTime;
	SetActorLocation(NLocation);
}

/*
*	Set by tower
*/
void ABullet::SetDirection(FVector NDirection)
{
	Direction = NDirection;
}

/*
*	Set by tower
*/
void ABullet::SetDamage(int32 NDamage)
{
	Damage = NDamage; // or use default
}

/*
*	Set by tower
*/
void ABullet::SetDamageScore(int32 Points)
{
	DamageScore = Points; // or use default
}

/*
*	If bullet detects an actor, check if it's an enemy.
*	If so, cause damage to that enemy and despawn self.
*	Damage to enemies earns player points.
*	If not an enemy, just ignore.
*/
void ABullet::OnCapsuleOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, 
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!OtherActor)
		return;

	ABaseEnemy* Enemy = Cast<ABaseEnemy>(OtherActor);
	if (Enemy)
	{
		Enemy->TakeHit(Damage);

		// give player points for damage
		ATDGameMode* Gmode = Cast<ATDGameMode>(GetWorld()->GetAuthGameMode());
		Gmode->UpdateScore(DamageScore);

		// Deactive particles  - Just incase
		BulletSparks->DeactivateSystem();
		BulletSparks->DestroyComponent();

		Destroy();
	}
}
