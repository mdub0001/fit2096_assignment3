// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// defaults
	SpawnInterval = 3;	// Spawn enemy every 3 seconds
	SpawnerCountDown = SpawnInterval;
	StartSpawning = true;
	NumberEnemiesSpawned = 0;
	MaxSpawnCount = 20;	// Enemies spawned per level
	MaxNumberEnemiesOnScreen = 10;	// difficulty/flow control
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	GeneratePaths();
}

/*
*	While spawner is active, spawns a random enemy type each interval
*	until the maximum spawn count of that enemy is reached, after which
*	spawner deactivates.
*	Amount of enemies on screen is limited to performance/difficulty control.
*/
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// False when Max enemies spawn or between waves
	if (!StartSpawning)
		return;

	// Prevent crashes
	if (EnemyTypes.Num() == 0)
	{
		UE_LOG(LogTemp, Error, TEXT("No enemies selected in spawner"));
		Destroy();
	}

	// track time between spawns
	SpawnerCountDown -= DeltaTime;

	if (SpawnerCountDown <= 0)
	{
		// check number of enemies alive in level
		TArray<AActor*> OutActiveEnemies;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABaseEnemy::StaticClass(), OutActiveEnemies);

		// Limit attacking enemies for performance and difficulty control
		if (OutActiveEnemies.Num() < MaxNumberEnemiesOnScreen)
		{
			SpawnEnemy();
			NumberEnemiesSpawned++; // error here if max enemies 1?
		}

		// Level spawn phase Complete - Deactivate.
		if (NumberEnemiesSpawned >= MaxSpawnCount)
			StartSpawning = false;

		// reset timer
		SpawnerCountDown = SpawnInterval;
	}
}

/*
*	Spawn a random enemy type into the level.
*	Select and pass path/route for the enemy to
*	spawn at and follow.
*/
void ASpawner::SpawnEnemy()
{
	// Don't spawn enemy if none have been pre selected
	if (EnemyTypes.Num() > 0)
	{
		// select random enemy and Spawn point
		int32 RandEnemy = FMath::RandRange(0, EnemyTypes.Num() - 1);
		int32 RandPath = FMath::RandRange(0, EnemyPaths.Num() - 1);

		// spawn enemy
		ABaseEnemy* temp = GetWorld()->SpawnActor<ABaseEnemy>(
				EnemyTypes[RandEnemy],
				EnemyPaths[RandPath].Waypoints[0], // Spawn position is first route waypoint
				FRotator::ZeroRotator
			);
		// Set initial positions and paths
		temp->InitializePaths(&EnemyPaths, RandPath, 6);
	} 
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No enemy types selected for spawner."));
	}
}

/*
*	Generate waypoints for enemies to navigate to.
*	Waypoint map will be sent to enemies when spawned.
*/
void ASpawner::GeneratePaths()
{
	int Ztemp = StartWaypoint.Z;
	int Ytemp = StartWaypoint.Y;

	// Generate Path indexes
	for (int i = 0; i < PathNumber; i++)
	{
		FPath p;
		int Xtemp = StartWaypoint.X;

		// Generate Waypoint Vectors
		for (int j = 0; j < WaypointNumber; j++)
		{
			p.Waypoints.Add(FVector(Xtemp, Ytemp, Ztemp));
			Xtemp += IncrementX;
		}

		EnemyPaths.Add(p);
		Ytemp -= DecrementY;
	}
}
