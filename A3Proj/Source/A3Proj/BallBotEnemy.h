// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseEnemy.h"
#include "BallBotEnemy.generated.h"

/**
 *	Ball bot is a huge enemy, with loads of 
 *	Health, slow speeds and big damage.
 */
UCLASS()
class A3PROJ_API ABallBotEnemy : public ABaseEnemy
{
	GENERATED_BODY()

	protected:
		virtual void BeginPlay() override;

	public:
		ABallBotEnemy();
		virtual void Tick(float DeltaTime) override;	
};
