// Fill out your copyright notice in the Description page of Project Settings.


#include "TDGameMode.h"

ATDGameMode::ATDGameMode()
{
	PrimaryActorTick.bCanEverTick = true;
	TimeLimit = 60;
}

void ATDGameMode::BeginPlay()
{
	Super::BeginPlay();
	Score = 0;
	IsGameOver = false;
	TimeRemaining = TimeLimit;
	EnemyKillCount = 0;
	HasWon = true; // set to false on ship destruction
}

void ATDGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimeRemaining -= DeltaTime;

	if (TimeRemaining <= 0)
		IsGameOver = true;
}

/*
*	Points earned is determined by the enemy.
*	Points can be earned through successful damage dealt
*	to enemy, or upon enemy deaths (not self destruction)
*/
void ATDGameMode::UpdateScore(int32 EarnedPoints)
{
	Score += EarnedPoints;
}

/*
*	Only called by the ship, 
*	therefore can assume the game is over if called.
*/
void ATDGameMode::SetGameOver(bool IsOver)
{
	HasWon = false;
	IsGameOver = IsOver;
}

void ATDGameMode::IncrementEnemyKillCount()
{
	EnemyKillCount++;
}

