// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseEnemy.h"
#include "BeeBotEnemy.generated.h"

/**
 *	Bee bot is an average enemy with averge speed
 *	and damage. He is pretty cute though.
 */
UCLASS()
class A3PROJ_API ABeeBotEnemy : public ABaseEnemy
{
	GENERATED_BODY()
	
	protected:
		virtual void BeginPlay() override;

	public:
		ABeeBotEnemy();
		virtual void Tick(float DeltaTime) override;	
};
