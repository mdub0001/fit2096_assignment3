// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseEnemy.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "TDGameMode.h"
#include "BaseTower.h"
#include "Materials/MaterialInstanceDynamic.h"

ABaseEnemy::ABaseEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// for child classes to add their meshes
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	Scene->SetupAttachment(RootComponent);
	EnemyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	EnemyMesh->SetupAttachment(Scene);
	EnemyCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Enemy Collision"));
	EnemyCollision->SetupAttachment(Scene);

	// Get particles and sound for explosion
	ConstructorHelpers::FObjectFinder<UParticleSystem> ExplosionPath = TEXT("/Game/StarterContent/Particles/P_Explosion.P_Explosion");
	if (ExplosionPath.Succeeded())
		EnemyExplosion = ExplosionPath.Object;

	ConstructorHelpers::FObjectFinder<USoundCue> ExplosionSoundPath = TEXT("/Game/StarterContent/Audio/Explosion_Cue.Explosion_Cue");
	if (ExplosionSoundPath.Succeeded())
		ExplosionSound = ExplosionSoundPath.Object;

	// defaults;
	MinimumDistance = 50;	// until target waypoint
	WaypointNumber = 1; // spawned at waypoint[0]
	MaxWaypointNumber = 6;
	PathNumber = 0;
	Acceleration = 0;	// determined by Lerp
	MaxSpeed = 50;
	CurrentSpeed = 0;
	MaxSize = 3;
	Size = 1;
	IsTurning = true; // stop moving when turning
	TurnThreshold = 0.01;
	TurnSpeed = 4;
	ExplosionDamage = 10;
	DeathScorePoints = 5;
}

// Called when the game starts or when spawned
void ABaseEnemy::BeginPlay()
{
	Super::BeginPlay();
	// Set speed based on size
	this->SetActorScale3D(FVector(Size, Size, Size));
	Speed = (1 - (Size / MaxSize)) * MaxSpeed;
	Health = MaxHealth;

	// set up material instace for colour change
	EnemyMaterial = UMaterialInstanceDynamic::Create(EnemyMesh->GetMaterial(0), NULL);
	EnemyMesh->SetMaterial(0, EnemyMaterial);
	EnemyCollision->OnComponentBeginOverlap.AddDynamic(this, &ABaseEnemy::OnBoxOverlapBegin);
}

/*
*	While the enemy has a waypoint to navigate to,
*	the enemy faces the direction of the waypoint and
*	moves towards it.
*	When it is within a threshold distance, the destination changes
*	to the next waypoint.
*	Once at waypoint, gradualy turn to face the next waypoint, then continue.
*	Once the enemy has run out of waypoints, the enemy has reached the base 
*	and explodes, damaging the ship.
*	If the enemy overlaps with a tower actor, the enemy will self destruct, 
*	dealing damage to the tower.
*/
void ABaseEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// if waypoints havn't been set or ship collisions incorrect
	if (Paths == nullptr || WaypointNumber >= MaxWaypointNumber)
	{
		Die();
		return;
	}

	// detemrine target destination
	FVector TargetWaypiont = (*Paths)[PathNumber].Waypoints[WaypointNumber];
	FVector TargetDirection = TargetWaypiont - this->GetActorLocation();
	TargetDirection.Normalize();

	// if at waypoint, turn to next one
	if (IsTurning)
	{
		// Add incremental turn to current facing direction
		FVector Forward = GetActorForwardVector();
		AddActorLocalRotation(FQuat(FRotator(0, (TargetDirection.Y - Forward.Y) * TurnSpeed, 0)));

		// When actor is facing new target, move forward
		if (FMath::Abs(TargetDirection.Y - Forward.Y) < TurnThreshold)
		{
			IsTurning = false;
			CurrentSpeed = Speed;
		}
	} 
	else
	{
		// Move towards target waypoint
		Acceleration = FMath::Lerp(Acceleration, CurrentSpeed, DeltaTime * 0.5);
		this->SetActorLocation(this->GetActorLocation() + TargetDirection * Acceleration);
	}

	// At target waypoint, stop acceleration and turn to next waypoint
	if (FVector::Dist(this->GetActorLocation(), TargetWaypiont) < MinimumDistance)
	{
		DecidePath();
		WaypointNumber++;
		Acceleration = 0;
		CurrentSpeed = 0;
		IsTurning = true;
	}
}

/*
*	Determine if enemy should continue straight on their
*	current path, or take a diagonal route to a neighbouring path.
*/
void ABaseEnemy::DecidePath()
{
	// 20% chance to change path
	int32 Change = FMath::RandRange(0, 9);
	if (Change <= 1)
	{
		// turn right
		if (PathNumber == 0)
			PathNumber++;
		// turn left
		else if (PathNumber == 3)
			PathNumber--;
		// turn right
		else if (Change == 1)
			PathNumber++;
		// turn left
		else
			PathNumber--;
	}
}

/*
*	Check if another actor is overlapping on this enemies collision volume
*	and act accordingly.
*	If actor is a tower or the ship, explode self and deal damage to those actors.
*/
void ABaseEnemy::OnBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, 
UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{	
	if (!OtherActor || OtherActor == this)
		return;
	
	ABaseTower* Tower = Cast<ABaseTower>(OtherActor);
	if (Tower)
	{
		Tower->TakeHit(ExplosionDamage);
		Die();
	}

	ASpaceshipBase *Ship = Cast<ASpaceshipBase>(OtherActor);
	if (Ship)
	{
		Ship->TakeHit(ExplosionDamage);
		Die();
	}
}

/*
*	Set the initial path values upon spawn.
*	Includes setting the map, the path number spawned on and the
*	last waypoint until destination.
*/
void ABaseEnemy::InitializePaths(TArray<FPath> *MapPaths, int32 nPathNumber, int32 nMaxWaypointNumber)
{
	Paths = MapPaths;
	PathNumber = nPathNumber;
	MaxWaypointNumber = nMaxWaypointNumber;
}

/*
*	Recieve damage to health from towers.
*	If health falls below 0, explode and destroy without damaging
*	the player towers or ship.
*	Provide points to the player upon death.
*/
void ABaseEnemy::TakeHit(int32 Damage)
{
	if (Damage >= Health)
	{
		ATDGameMode* Gmode = Cast<ATDGameMode>(GetWorld()->GetAuthGameMode());
		Gmode->UpdateScore(DeathScorePoints); // MUST be killed by damage to recieve points
		Gmode->IncrementEnemyKillCount();
		Die();
		return;
	}

	Health -= Damage;
	// change colour of material upon health loss
	float matval = (float(Health) / MaxHealth) * 1;
	EnemyMaterial->SetScalarParameterValue(ColourChangeAttribute, matval);
}

/*
*	Spawn the explosion particles and sounds upon death,
*	then destroy self.
*/
void ABaseEnemy::Die()
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EnemyExplosion, GetActorTransform(), true, EPSCPoolMethod::AutoRelease, true);
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSound, GetActorLocation(), FRotator::ZeroRotator, 1, 1, 0, nullptr, nullptr, false);
	Destroy();
}

