// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "A3ProjGameMode.generated.h"

UCLASS(minimalapi)
class AA3ProjGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AA3ProjGameMode();
};



