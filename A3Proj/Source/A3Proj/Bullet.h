// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/CapsuleComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "BaseEnemy.h"
#include "Bullet.generated.h"

/*
*	Projectile actor that is spawned from the turret towers.
*	Travels in a straigh direction with the specified speed.
*	Particle effects trail is attached to bullet while alive.
*	If bullet makes contact with an enemy, bullet deals damage
*	that was specified by the tower.
*	If no contact, then the bullet dies after a certain distance.
*/
UCLASS()
class A3PROJ_API ABullet : public AActor
{
	GENERATED_BODY()
	
	AActor* Target;
	FVector Direction;
	FVector SpawnLocation;
	int32 DamageScore;

public:	
	// Sets default values for this actor's properties
	ABullet();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* BulletMesh;

	UPROPERTY(EditAnywhere)
		UParticleSystemComponent* BulletSparks; // pretty bullet trail

	UPROPERTY(EditAnywhere)
		UCapsuleComponent* BulletCollision;

	UPROPERTY(EditAnywhere)
		USoundCue* ShootSound;

	UPROPERTY(BlueprintReadWrite) 
		int32 Damage; // should be set through enemy shooter

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float LifeRange; // until bullet despawns

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void SetDirection(FVector NDirection);
	void SetDamage(int32 NDamage);
	void SetDamageScore(int32 Points);

	UFUNCTION()
	void OnCapsuleOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
