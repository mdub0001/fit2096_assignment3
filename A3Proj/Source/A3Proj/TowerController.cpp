// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerController.h"

ATowerController::ATowerController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Default
	MapZAxis = 200;
	RandomLocMin = FVector(-1931, -1350, MapZAxis);
	RandomLocMax = FVector(1000, 1929, MapZAxis);
	RandomTowerPlacement = false;
	SpawnCooldown = 3;
	SelectedTowerType = 0;
}

// Called when the game starts or when spawned
void ATowerController::BeginPlay()
{
	Super::BeginPlay();
	CooldownCurrent = SpawnCooldown;
	CanSpawn = false;
}

// Called every frame
void ATowerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CooldownCurrent -= DeltaTime;

	if (!CanSpawn && CooldownCurrent < 0)
		CanSpawn = true;

}

/*
*	Called when player clicks screen.
*	Placements tower depending on placement method
*	(manual or random).
*	Towers can only be placed within a specified boundary.
*/
bool ATowerController::SpawnTower()
{
	// Check if spawn cooldown is active
	if (!CanSpawn)
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawner in cooldown"));
		return false;
	}

	// Set spawn location
	FVector SpawnLoc;
	if (RandomTowerPlacement)
	{
		SpawnLoc = GetRandomLocation();
	}
	else
	{
		SpawnLoc = GetLocationByCursor();
		// check selected spawn location is within map limits
		if (SpawnLoc.X < RandomLocMin.X || SpawnLoc.X > RandomLocMax.X || SpawnLoc.Y < RandomLocMin.Y || SpawnLoc.Y > RandomLocMax.Y)
		{
			UE_LOG(LogTemp, Error, TEXT("Chosen spawn location is outside the Map. Trying again."));
			return false;
		}
	}

	// Spawn tower
	GetWorld()->SpawnActor<ABaseTower>(TowerTypes[SelectedTowerType], SpawnLoc, FRotator::ZeroRotator);

	// reset timer
	CooldownCurrent = SpawnCooldown;
	CanSpawn = false;
	return true;
}

/*
*	Get the players cursor position and convert to
*	world place to act as a spawning position.
*/
FVector ATowerController::GetLocationByCursor()
{
	FHitResult Hit;
	GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, false, Hit);
	Hit.Location.Z = MapZAxis; // to prevent spawning under map
	return Hit.Location;
}

/*
*	Generates random location values for spawning.
*/
FVector ATowerController::GetRandomLocation()
{
	return FVector(
		FMath::RandRange(RandomLocMin.X, RandomLocMax.X), 
		FMath::RandRange(RandomLocMin.Y, RandomLocMax.Y), 
		RandomLocMax.Z
	);
}

