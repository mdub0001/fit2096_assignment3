// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
//#include "BaseTower.h"
#include "TurretTower.h"
#include "AOETower.h"
#include "TowerController.generated.h"

/*
*	Responsible for spawning towers.
*	Holds the different tower types that can be spawned within the game.
*	Allows the player to spawn towers via mouse click
*	or randomly.
*/
UCLASS()
class A3PROJ_API ATowerController : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<ABaseTower>> TowerTypes;

	// boundaries for random placement
	FVector RandomLocMin;
	FVector RandomLocMax;
	float MapZAxis; // Landscape isn't on 0,0,0
	
public:	
	// Sets default values for this actor's properties
	ATowerController();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SpawnCooldown;

	UPROPERTY(BlueprintReadonly)
		float CooldownCurrent;
	bool CanSpawn;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	FVector GetRandomLocation();
	FVector GetLocationByCursor();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Blueprintcallable, Category="Custom")
		bool SpawnTower();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (UIMin = '0', UIMax = '1'))
		int32 SelectedTowerType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool RandomTowerPlacement;

};
