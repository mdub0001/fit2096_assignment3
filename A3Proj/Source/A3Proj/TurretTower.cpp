// Fill out your copyright notice in the Description page of Project Settings.


#include "TurretTower.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"


ATurretTower::ATurretTower()
{
	PrimaryActorTick.bCanEverTick = true;
	ConstructorHelpers::FObjectFinder<UStaticMesh> TurretMeshPath(TEXT("/Game/Geometry/Meshes/TurretTower.TurretTower"));

	if (TurretMeshPath.Succeeded())
		TowerMesh->SetStaticMesh(TurretMeshPath.Object);

	DetectionVolume = CreateDefaultSubobject<USphereComponent>(TEXT("Tower Detection"));
	DetectionVolume->SetupAttachment(Scene);

	// Defaults
	TurnSpeed = 1;
	TurnThreshold = 0.01;
	RateOfFire = 3;
	AimOffset = 0;
}

void ATurretTower::BeginPlay()
{
	Super::BeginPlay();
	CollisionVolume->OnComponentBeginOverlap.AddDynamic(this, &ATurretTower::OnSphereOverlapBegin);
	CollisionVolume->OnComponentEndOverlap.AddDynamic(this, &ATurretTower::OnSphereOverlapEnd);
	DetectionVolume->SetSphereRadius(DetectionRadius);
	CoolDownTimer = RateOfFire;
}

void ATurretTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CoolDownTimer -= DeltaTime;

	if (!IsValid(Target))
		return;

	// calculate angle to face target
	FVector TargetDirection = Target->GetActorLocation() - this->GetActorLocation();
	FVector Forward = GetActorForwardVector();
	float Cross = Forward.X * TargetDirection.Y - Forward.Y * TargetDirection.X;

	// determines shorts turn route
	if (Cross > 0)
		AddActorLocalRotation(FQuat(FRotator(0, TurnSpeed, 0)));
	else
		AddActorLocalRotation(FQuat(FRotator(0, TurnSpeed*-1, 0)));

	// Shoot if not in cooldown
	if (CoolDownTimer < 0)
	{
		Shoot();
		CoolDownTimer = RateOfFire;
	}
}

/*
*	Spawn a bullet actor directing it to the target that
*	the enemy is facing.
*/
void ATurretTower::Shoot()
{
	if (!Bullet)
	{
		UE_LOG(LogTemp, Error, TEXT("No bullet set"));
		return;
	}

	// spawn the bullet
	ABullet *temp = GetWorld()->SpawnActor<ABullet>(Bullet, GetActorLocation(), FRotator::ZeroRotator);

	// apply randomness to accuracy
	float AimShift = GetAimOffset();
	FVector AimDirection = GetActorForwardVector();
	AimDirection.X += AimShift;
	AimDirection.Y += AimShift;

	// set bullet parameters
	temp->SetDirection(AimDirection);
	temp->SetDamage(FMath::RandRange(MinAttackDamage, MaxAttackDamage));
	temp->SetDamageScore(DamageScore);
}

/*
*	Generates randomness based on the define aim offset.
*	Offset is value between 0 - 1, with 0 being 100% accurate
*	and closer to 1 being a terrible shot.
*
*	@returns: the random offset value.
*/
float ATurretTower::GetAimOffset()
{
	float ShiftAmount = FMath::RandRange(AimOffset * -1, AimOffset);
	return ShiftAmount;
}

/*
*	Checks any actors that enter the trigger volume which
*	defines this towers range.
*	If that actor is an enemy, store them in an array.
*	If this tower has no target, this actor now becomes the target.
*	If the current enemy dies, use array to switch to another target
*	within range.
*/
void ATurretTower::OnSphereOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, 
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!OtherActor || OtherActor == this)
		return;

	// If the actor is an enemy, track as potential target
	ABaseEnemy *Enemy = Cast<ABaseEnemy>(OtherActor);
	if (Enemy)
	{
		// new target
		if (Target == nullptr)
			Target = Enemy;
		else
			InRangeEnemies.Add(Enemy); // switch to actor later if current target dies
	}
}

/*
*	Tracks when an actor leaves the range of sight.
*	If actor was the target, attempt to switch to a new one
*	if possible.
*	If not the current target, then forget about actor, remove from array.
*/
void ATurretTower::OnSphereOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (Target == nullptr || !OtherActor)
		return;

	ABaseEnemy *Enemy = Cast<ABaseEnemy>(OtherActor);
	if (!Enemy)
		return;

	if (Enemy == Target)
	{
		Target = NULL;

		if (InRangeEnemies.Num() > 0)
		{ // switch target
			Target = InRangeEnemies[0];
			InRangeEnemies.RemoveAt(0);
		}
	} 
	else
	{ // has target, remove from array
		if (InRangeEnemies.Contains(Enemy))
			InRangeEnemies.Remove(Enemy);
	}
}
