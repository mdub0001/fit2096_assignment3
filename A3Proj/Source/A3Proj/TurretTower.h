// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseTower.h"
#include "Bullet.h"
#include "TurretTower.generated.h"

/**
*	When an enemy is within range, turns self around to
*	face that enemy and shoots.
*	Shots have a chance to miss based on the aimoffset value.
*	Shots produce a bullet actor.
*/
UCLASS()
class A3PROJ_API ATurretTower : public ABaseTower
{
	GENERATED_BODY()

	ABaseEnemy* Target;
	int32 TurnSpeed;
	float TurnThreshold = 0.01;
	
	protected:
		virtual void BeginPlay() override;

	public:
		ATurretTower();
		virtual void Tick(float DetlaTime) override;
		virtual void Shoot();
		float GetAimOffset();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<ABullet> Bullet;

	UPROPERTY(EditAnywhere)
		USphereComponent* DetectionVolume;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float AimOffset; // accuracy

	UFUNCTION()
		void OnSphereOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
		void OnSphereOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
