// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "TDGameMode.generated.h"

/**
 *	Game mode for the Tower Defence game.
 *	Handles stats such as score and enemies killed as
 *	well as the win conditions.
 *	Handles the game timer.
 *	If Timers runs out without Ship destruction, player wins.
 */
UCLASS()
class A3PROJ_API ATDGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATDGameMode();
	virtual void Tick(float DeltaTime);
	void UpdateScore(int32 EarnedPoints);
	void SetGameOver(bool IsOver);
	void IncrementEnemyKillCount();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool IsGameOver;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float TimeLimit;

	UPROPERTY(BlueprintReadOnly)
		float TimeRemaining;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Score;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 EnemyKillCount;

	UPROPERTY(BlueprintReadOnly)
		bool HasWon; // to display win screen

protected:
	virtual void BeginPlay() override;
};
