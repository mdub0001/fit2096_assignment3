// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "BaseTower.generated.h"

class ABaseEnemy;
class UMaterialInstanceDynamic;

/*
*	Base class for Towers within the game.
*	Towers are there to support the player's defence of the base
*	by seeking and shooting enemies.
*	They also double as meat sheilds.
*	Towers will slowly turn red as they lose health
*	Holds properties of the towers such as health, damage,
*	Mesh, range and cooldowns.
*	Responsible for basic functionalities that are 
*	prelavent across all towers.
*/
UCLASS(ABSTRACT)
class A3PROJ_API ABaseTower : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ABaseTower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int32 Health;
	float CoolDownTimer; // cooldown
	TArray<ABaseEnemy*> InRangeEnemies;
	int32 DamageScore; // Points awarded to the player

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void TakeHit(int32 Damage);
	void Shoot();
	void Die();

	FName EmmisiveChangeAttribute = TEXT("Blend"); // allows material colour change
	UMaterialInstanceDynamic* TowerMaterial;

	// Damage ranges
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MaxAttackDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MinAttackDamage;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MaxHealth;

	// Controls cooldown time
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float RateOfFire;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DetectionRadius;

	UPROPERTY(EditAnywhere)
		UParticleSystem* TowerExplosion;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* TowerMesh;

	UPROPERTY(EditAnywhere)
		USphereComponent* CollisionVolume;

	UPROPERTY()
		USceneComponent* Scene;
};
