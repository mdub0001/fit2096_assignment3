# Manaloritron

## Can I see a demo?
A quick demo can be viewed here: https://www.youtube.com/watch?v=URXw1X4JibM&feature=youtu.be

I apologise for my terrible tower defence skills.

## How do I Find...
**Levels:** Content > ThirdPersonCPP > Maps

**Blueprints:** Content > ThirdPersonCPP > Blueprints

**GameMode Blueprint:** Content > Blueprints (sorry I accidently stored it in the wrong place!)

**Blueprint Widgets:** Content > ThirdPersonCPP > Blueprints > HUD

**Materials:** Content > Materials

**Textures:** Content > Textures

**Meshes:** Content > Geometry > Meshes

**Particle Effects:** Content > StarterContent > Particles (my custom one is P_BulletTrail, the others are repurposed default particles)

## Implemented Features
### Ship/Win conditions
- The destroyed spaceship acts as the base for this game, having a HP and can be damaged.
- Once ship runs out of HP, it is game over for the player.
- If the player manages to protect the ship for a full minute, the player wins.
- Points are given to the player each time their tower hits an enemy successfully, and additional points awarded if the damage kills the enemy.

### Waypoints
- Waypoints are a list of FVectors contained within Structs due to UE unable to handle nesting...
- Enemies can navigate along a straight path or take a diagonal route to switch paths.

### Enemies
- Enemies are spawned randomly at the first waypoint.
- Spawn location and rate is detmined by spawner.
- All attributes for enemies can be set in editor/Blueprint
- The size of the enemy determines the speed.
- Enemy Materials change from Blue to Red gradually as they take damage.
- Enemies explode on death but also self destruct on collision with a tower or the shipbase, damaging them.
- Note that when enemies self explode, the player does not recieve points.

### Towers
- TowerController controls the spawning of towers.
- Cooldown is applied to controller to prevent player from spamming towers. This attribute can be controllers via the editor.
- Towers can be spawnned manually or randomly (see additional functionality section)
- Towers can lose HP and be destroyed. Towers material colour will also change on HP loss.
- Towers damage the enemy bots either via AOE or shooting a bullet.
- Tower has chance to miss with AimOffset which acts as accuracy. Value should be 0 - 1, 0 being 100% acurate, 1 being an awful shot.

## Bullets
- Deal damage based on the tower.
- Has trailing particle affects.

## Additional Functionality/Stuff
- The player is given the option of placing towers randomly or manually.
- Player can place towers where they wish by ensuring the tower they want is selected at the top of the UI and clicking within a valid area. Non-valid area will notify the player.
- Created the landscape for the game, which included using the UE4 landscaping tool as well as learning how to create a layered material for terrain texturing.
- Created a material which affects the viewport to make it look like hot desert haze (look at references for source)
- Created a Decal texture and spawned a Decal beneath the AOETower to indicate range (Note the range of the decal is not 100% acurate as I couldn't find a way to match it to the spawned sphere)
- Created a particle affect from scratch to create a trail of light connected to the bullet actors.
- Created a few meshes, created custom textures and materials
- Animated a few of the UI elements such as the starting screen intro and the invalid messages.

## Notes to Marker
- In the unlikely event a blueprint runtime error occours in the log box, or the UI isn't responding/updating, a hot reload/compile of the GameKeys Blueprint and level blueprint is required: https://issues.unrealengine.com/issue/UE-29768
- Unfortunately ran out of time, would have liked to add cooldowns for specific towers.
- I chose not to have my towers/enemies different colours even though the breif states to. But their mateirals are different, I hope that still counts. Just makes sense for the enemies to be one colour and the towers to be another!
- I notice the brief says particles should spawn when the bullet spwns and hits, I've not implemented the hit particle effect as when I did it was just covered up with the explosion of the Enemy bot.
- After completing this, I realise that the game could have been a lot better if the map size had been bigger. I apologise, I don't have much expierence with tower defence game, so I was improvising!

## References
All non-starter content meshes, textures and materials were created by myself during the course of this project or from previous projects:
- Monash University - FIT1033 (2018)
- University of Hertfordshire - Thorn (2013)
- My own person projects

**Learnt the scene material method for desert haze from:** https://www.youtube.com/watch?v=BRi4KrGCS80

**Sound Effects under the creative commons liscene from Freesound.org:**
https://freesound.org/people/UdoPohlmann/sounds/132880/
https://freesound.org/people/Burning_Cicada_Ltd./sounds/496320/
https://freesound.org/people/pepingrillin/sounds/252083/
https://freesound.org/people/Nox_Sound/sounds/466044/
